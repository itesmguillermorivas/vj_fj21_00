﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    // public attribute
    public float speed = 5;
    public Text text;
    
    // To access other components we need a reference to them
    private Transform t;


    // LIFECYCLE! 
    // we have no access to main method
    // we can inject logic to particular game objects on specific moments of its life


    // awake is run upon component creation
    // only once
    // awake always runs
    void Awake()
    {
        print("AWAKE");
        
        // get a reference to any component IN THE SAME GAME OBJECT
        t = GetComponent<Transform>();
        
    }

    // start is also called once after ALL the awake methods have been called
    // start only runs in objects that are enabled
    // Start is called before the first frame update
    void Start()
    {
        //Debug.Log("START");

        
    }

    // Update is called once per frame
    // the game engine runs in a loop! (and pretty much any graphical app does)
    // loop does 2 things: run logic, update graphics (frame)
    // the idea is: run the loop as many times as you can in a second
    // amount of frames per second (fps) = framerate
    // ideal - 60+ fps
    // ok - 30 fps (the least fps to consider an app real time)

    // 1 rule for update: do only the following:
    // - check input from user
    // - move stuff
    void Update()
    {

        // how to capture input 
        // poll directly to the devices
        /*
        if(Input.GetKeyDown(KeyCode.A)){
            print("DOWN");
        }

        if(Input.GetKey(KeyCode.A)){
            //print("KEY");
        }

        if(Input.GetKeyUp(KeyCode.A)){
            print("UP");
        }

        if(Input.GetMouseButtonDown(0)){

            print("MOUSE DOWN");
        }
        
        if(Input.GetMouseButton(0)){

            print("MOUSE");
        }

        if(Input.GetMouseButtonUp(0)){

            print("MOUSE UP");
        }
        // Debug.Log("UPDATE");
        */

        // using axes 
        // retrieve a float value
        // 0 is neutral
        // range = [-1, 1]
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        // move the object
        // delta time: amount of time that passed between the last frame and this one
        transform.Translate(speed * h * Time.deltaTime, speed * v * Time.deltaTime, 0, Space.World);


        
    }

    // late update happens in each frame 
    // after ALL the updates
    void LateUpdate(){

        //Debug.Log("LATE UPDATE");
    }


    // will run in a set amount of time 
    void FixedUpdate(){

        //Debug.Log("FIXED UPDATE");        
    }

    // collisions between objects using the physics engine
    // requirements:
    // - all the objects involved MUST have a collider
    // - at least one of them has a rigidbody component (subscribes object to physics engine)
    // - the rigidbody enabled object must be moving (otherwise is considered asleep and not taken into account)


    // three moments in the life of a collision

    // invoked in the frame in which 2 objects start touching
    void OnCollisionEnter(Collision c)  {

        // Collision is an object that contains info of the collision!
        // such as:
        // - a reference to the other object involved
        // - points of collision
        // etc
        print("COLLISION IS WORKING " + c.transform.name);
        print("COLLISION IS WORKING " + c.transform.tag);
        print("COLLISION IS WORKING " + c.transform.gameObject.layer);
    }

    // invoked after the frame that they started touching and while they still touch
    void OnCollisionStay(Collision c)  {

        //print("COLLISION STAY");
    }

    // invoked the first frame in which the objects stopped touching
    void OnCollisionExit(Collision c)  {

        //print("COLLISION EXIT");
    }


    // TRIGGER
    // you can use triggers if you don't want any physical reaction but still want to be able to interact with an object
    void OnTriggerEnter(Collider c){

        print("TRIGGER ENTER " + c.transform.name);
        print("TRIGGER ENTER " + c.transform.tag);
        print("TRIGGER ENTER " + c.transform.gameObject.layer);
        text.text = "A TRIGGER HAPPENED!";
    }

    void OnTriggerStay(Collider c){

        //print("TRIGGER STAY");
    }

    void OnTriggerExit(Collider c){

        //print("TRIGGER EXIT");
    }
}
